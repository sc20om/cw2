#include <stdio.h>
#include <stdlib.h>
#include "../header/MapStructures.h"

Node* createNodePointer(long id, float latitude, float longitide, unsigned int numConns, Link* connList)
{
    Node* node = malloc (sizeof(Node));
    node->id = id;
    node->latitude = latitude;
    node->longitide = longitide;
    node->numConns = numConns;
    node->connList = connList;

    return node; 
}

void addToNodeArray(Node* array, int newSize, Node node)
{
    array = realloc(array, newSize * sizeof(Node));
    array[newSize-1] = node;
}

Link* createLinkPointer(long linkId, long destNodeId, float length)
{
    Link* link= malloc (sizeof(Link));
    link->linkId = linkId;
    link->destNodeId = destNodeId;
    link->length = length;

    return link; 
}

Map* createMapPointer(Node* nodeArray, unsigned int length)
{
    Map* map = malloc(sizeof(Map));
    map->nodeArray = nodeArray;
    map->length = length;

    return map;
}

int freeMap(Map* map)
{
    int i;
    int j;
    for (i = 0; i < map->length; i++)                       
    {
        free(map->nodeArray[i].connList);           //Free every Link associated with each node
    }
    free(map->nodeArray);                               //Free every node associated with the map
    free(map);                                          //Free the map
}

Node* getNodeFromId(long id, Map* g_map)
{
    int i;
    for (i = 0; i < g_map->length; i++)
    {
        if (g_map->nodeArray[i].id == id)
        {
            return &g_map->nodeArray[i];
        }        
    }
    return NULL;    
}