/*
Project 2 - Route finding
This project asks you to compute the best path between 2 points on a map.
You are given a large data set (to be downloaded from Minerva) that represents all the footpaths on the
Leeds University Campus that you can use to test your application.


1. You should create a suitable data structure for the problem - a suitable candidate would be an Adjacency
List which stores a list of points in the data, and for every point a list of points that are connected to it with
edges.
https://www.khanacademy.org/computing/computer-science/algorithms/graphrepresentation/a/representing-graphs

2. You should consider how to import the data into your data structure.

3. You should consider a suitable visualisation of the data - for example, edges can be plotted
in Gnuplot (see attached jpeg).

4. You should implement at least one algorithm that finds a path between 2 given points.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../header/ImportData.h"
#include "../header/DisplayMap.h"
#include "../header/FindShortestPath.h"

char divider[] = "================================================\n";

Map* g_map;

int main ()
{    
    int running = 1;
    while (running)
    {
        printf("\n");
        printf("%s", divider);
        printf("1)  Load map\n");
        printf("2)  Show map\n");
        printf("3)  Find shortest path\n");
        printf("4)  Quit\n");
        
        char line[256];
        int i;
        if (fgets(line, sizeof(line), stdin)) 
        {
            if (!strcmp(line, "1\n"))
            {
                g_map = import_data();
                continue;
            }
            if (!strcmp(line, "2\n"))
            {
                // printNodesAndLinksText(g_map);
                printNodesAndLinks(g_map);
                continue;
            }
            if (!strcmp(line, "3\n"))
            {
                shortest_path(g_map);
                continue;
            }
            if (!strcmp(line, "4\n"))
            {
                printf("Ending program\n");
                exit(EXIT_SUCCESS);
            }
        }        
    }
}
    /*  Todo
        Menu for user to select what to do from:

            Import File Data:  
                Put nodes into array of node structs
                Put links into array of link structs
                    Add each link to relevant node's ConnectionList  
            
            Path Finding Algorithm
                Implement Dijkstra's Algorithm

            Map Visualisation
                Gnuplot to plot edges

            
    */