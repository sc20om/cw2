#include "../header/DisplayMap.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../header/MapStructures.h"

char dividerData[] = "------------------------------------------------\n";

//Code adapted from: https://stackoverflow.com/questions/3521209/making-c-code-plot-a-graph-automatically
void printNodesAndLinks(Map* g_map)
{
    int numCommands = 2;
    char * commandsForGnuplot[] = {"set title \"Map\"", "plot \"data.temp\" using 1:2:(0.0000001)with circles, \"data.temp\" with lines lc rgb \"red\" lw 1"};
    // char * commandsForGnuplot[] = {"set title \"Map\"", "plot \"data.temp\" using 1:2:(0.0001)with circles, \"data.temp\" with lines lc rgb \"red\" lw 1"};
    // char * commandsForGnuplot[] = {"set title \"Map\"", "plot \"data.temp\" using 1:2:(0.0001)with circles"};
    FILE * temp = fopen("data.temp", "w");
    FILE * gnuplotPipe = popen ("gnuplot -persistent", "w");

    int i;
    for (i=0; i < g_map->length; i++)
    {
        int j;
        // printf("Id:%ld, long:%lf, lat:%lf \n", g_map->nodeArray[i].id, g_map->nodeArray[i].longitide, g_map->nodeArray[i].latitude);
        if (g_map->nodeArray[i].numConns > 0)
        {
            for (j=0; j < g_map->nodeArray[i].numConns; j++)
            {
                fprintf(temp, "%lf %lf\n", g_map->nodeArray[i].longitide, g_map->nodeArray[i].latitude);
                Node* destNode = getNodeFromId(g_map->nodeArray[i].connList[j].destNodeId, g_map);
                // printf("Id:%ld, long:%lf, lat:%lf \n", destNode->id, destNode->longitide, destNode->latitude);
                fprintf(temp, "%lf %lf\n", destNode->longitide, destNode->latitude);
            }    
            fprintf(temp, "\n");
        }        
        // printf("\n");
    }
    fclose(temp);

    for (i=0; i < numCommands; i++)
    {
        fprintf(gnuplotPipe, "%s \n", commandsForGnuplot[i]);
    }

    fflush(gnuplotPipe);
    fclose(gnuplotPipe);
}

void printNodesAndLinksText(Map* g_map)
{
    printf(dividerData);
    int i;
    for (i = 0; i < g_map->length; i++)
    {
        printf("NODE:\n");
        printf("id = %ld\n", g_map->nodeArray[i].id);
        printf("latitude = %.6f\n", g_map->nodeArray[i].latitude);
        printf("longitide = %.6f\n\n", g_map->nodeArray[i].longitide);

        int j;
        for (j = 0; j < g_map->nodeArray[i].numConns; j++)
        {
            printf("LINKS:\n");
            printf("Connected to node with id: %ld\n", g_map->nodeArray[i].connList[j].destNodeId);
            printf("With length: %.6f\n\n", g_map->nodeArray[i].connList[j].length);            
        }        
        printf(dividerData);
    }   
}