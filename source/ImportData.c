#include "../header/ImportData.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "../header/MapStructures.h"

Map* g_map;

int load_nodes(FILE *file)
{
    int running = 1;
    char buffer[MAXCHAR];
    char c;
    int size = 0;

    Node* nodeArray = malloc (sizeof(Node));   

    while (running == 1)
    {
        char type[MAXCHAR];
        long id;
        float latitude;
        float longitide;
        strcpy(type, get_type(file, type));

        if (strcmp(type, ""))
        {
            if (!strcmp(type, "<node"))
            {
                //Get id
                while ((c = fgetc(file)) != '='){}
                while ((c = fgetc(file)) != ' ')
                {
                    strncat(buffer, &c, 1);
                }
                id = atol(buffer);
                strcpy(buffer, "");

                //Get lat
                while ((c = fgetc(file)) != '='){}
                while ((c = fgetc(file)) != ' ')
                {
                    strncat(buffer, &c, 1);
                }
                latitude = atof(buffer);
                strcpy(buffer, "");

                //Get long
                while ((c = fgetc(file)) != '='){}
                while ((c = fgetc(file)) != ' ')
                {
                    strncat(buffer, &c, 1);
                }
                longitide = atof(buffer);
                strcpy(buffer, "");

                Node node = {id, latitude, longitide, 0 , NULL};
                size++;
                addToNodeArray(nodeArray, size, node);            
            }
            if (skip_line(file) == 1)
            {
                running = 0;
            }            
        } else
        {
            running = 0;
        }  
    }
    if (size > 0)
    {
        g_map = NULL;
        g_map = createMapPointer(nodeArray, size);
        printf("Num Nodes = %d\n", g_map->length);
        return 0;     
    }      
    return 1;
}

char* get_type(FILE *file, char type[MAXCHAR])
{
    char buffer[MAXCHAR];
    char c;
    int corruptCounter = 0;
    while ((c = fgetc(file)) != ' ' && c != EOF)
    {       
        corruptCounter++;
        if (corruptCounter>12)
        {
            strcpy(buffer, "");
            strcpy(type, buffer);
            return type;
        }        
        strncat(buffer, &c, 1);
    }
    strcpy(type, buffer);
    strcpy(buffer, "");
    return type;
}

int skip_line(FILE *file)
{
    char c;
    while ((c = fgetc(file)) != '>')
    {
    }
    c = fgetc(file);
    if (c == EOF)
    {
        return 1;
    } else
    {
        return 0;
    }    
}

int load_links(FILE *file)
{
    rewind(file);
    int running = 1;
    char buffer[MAXCHAR];
    char c;
    int size = 0;  

    while (running == 1)
    {
        char type[MAXCHAR];
        long id;
        long node1Id;
        long node2Id;
        double length;
        strcpy(type, get_type(file, type));

        if (strcmp(type, ""))
        {
            if (!strcmp(type, "<link"))
            {
                //Get id
                while ((c = fgetc(file)) != '='){}
                while ((c = fgetc(file)) != ' ')
                {
                    strncat(buffer, &c, 1);
                }
                id = atol(buffer);
                strcpy(buffer, "");

                //Get node1Id
                while ((c = fgetc(file)) != '='){}
                while ((c = fgetc(file)) != ' ')
                {
                    strncat(buffer, &c, 1);
                }
                node1Id = atol(buffer);
                strcpy(buffer, "");

                //Get node2Id
                while ((c = fgetc(file)) != '='){}
                while ((c = fgetc(file)) != ' ')
                {
                    strncat(buffer, &c, 1);
                }
                node2Id = atol(buffer);
                strcpy(buffer, "");

                //Skip the way details
                while ((c = fgetc(file)) != '='){}
                while ((c = fgetc(file)) != ' '){}

                //Get length
                while ((c = fgetc(file)) != '='){}
                while ((c = fgetc(file)) != ' ')
                {
                    strncat(buffer, &c, 1);
                }
                length = atof(buffer);
                strcpy(buffer, "");

                Link link1 = {id, node2Id, length};
                if (addLinkToNode(node1Id, link1) == 1)
                {
                    return 1;
                }

                Link link2 = {id, node1Id, length};
                if(addLinkToNode(node2Id, link2) == 1)
                {
                    return 1;
                }
                size = size + 2;        
            }
            if (skip_line(file) == 1)
            {
                running = 0;
            }            
        } else
        {
            running = 0;
        }  
    }    
    printf("Num links = %d\n", size);
    return 0;
}

int addLinkToNode(long nodeId, Link link)
{
    Node* node = getNodeFromId(nodeId, g_map);
    if (node == NULL)
    {
        return 1;
    }
    
    if (node->numConns == 0)
    {
        Link* linkArray = malloc (1 * sizeof(Link)); 
        node->connList = linkArray;
        node->connList[0] = link;
        
    } else
    {
        node->connList = realloc(node->connList, (node->numConns+1) * sizeof(Link));
        node->connList[node->numConns] = link;
    }
    node->numConns++;
    return 0;
}

Map* return_map()
{
    return g_map;
}

Map* import_data()
{
    char filename[MAXCHAR];
    printf("Enter .map file name: ");
    fgets(filename, MAXCHAR, stdin);
    filename[strcspn(filename, "\n")] = 0;
 
    FILE *mapFile = fopen(filename, "r");
    if (mapFile == NULL){
        printf("Could not open file %s",filename);
        return NULL;
    } else
    {
        if (load_nodes(mapFile) != 0)
        {
            printf("Could not load nodes from %s", filename);
            return NULL;
        } else
        {
            if (load_links(mapFile) != 0)
            {
                printf("Could not load links from %s", filename);
                return NULL;
            } else
            {
                // printNodesAndLinks();
                return g_map;
            }
        }
    }    
}