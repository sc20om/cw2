#include "../header/FindShortestPath.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../header/MapStructures.h"
#define INFINTIY -9999

Distances* distances;
DijkstraNode* nodeArray;
NodeVisit* nodeVisitArray;
long* pathArray;
int currentPathLength=0;


Map* shortest_path(Map* map)
{

    printf("Enter id of source node: ");   
    char str[7] = "source";
    Node* sourceNode = getNodeFromUser(str, map);
    if (sourceNode == NULL)
    {
        return NULL;
    }    

    printf("Enter id of target node: ");
    char str2[7] = "target";
    Node* targetNode = getNodeFromUser(str2, map); 
    if (targetNode == NULL)
    {
        return NULL;
    }        
    
    //Create distances for each node and set all weight to infinity apart from the starting node which should be set to 0
    distances = malloc (sizeof(Distances));
    nodeArray = malloc (map->length * (sizeof(DijkstraNode)));

    nodeVisitArray = malloc (map->length * (sizeof(NodeVisit)));

    int numBadNodes = 0;    //Bad node is a node without any links - these are not helpful for finding path
    
    int i;
    for (i=0; i < map->length; i++)
    {
        if (map->nodeArray[i].numConns > 0)
        {
            if (sourceNode->id == map->nodeArray[i].id)
            {
                DijkstraNode node = {sourceNode->id, 0};
                nodeArray[i-numBadNodes] = node;

                NodeVisit nodeVisit = {map->nodeArray[i].id, true};
                nodeVisitArray[i-numBadNodes] = nodeVisit;
            }
            else
            {
                DijkstraNode node = {map->nodeArray[i].id, INFINTIY};
                nodeArray[i-numBadNodes] = node;

                NodeVisit nodeVisit = {map->nodeArray[i].id, false};
                nodeVisitArray[i-numBadNodes] = nodeVisit;
            }
        }
        else
        {
            numBadNodes++;
        }         
    }
    distances->dijkstraNodeArray = nodeArray;
    distances->length = map->length - numBadNodes;
    pathArray = malloc (distances->length * (sizeof(long))); 

    if (numBadNodes > 0)
    {
        printf("There are %d nodes without links in this map\n", numBadNodes);
    }
    dijkstra(sourceNode, targetNode);
}

Node* getNodeFromUser(char nodeType[], Map* map)
{
    long id;
    char line[256];
    if (fgets(line, sizeof(line), stdin)) 
    {
        id = atol(line);
        Node* node = getNodeFromId(id, map);
        if (node == NULL)
        {
            printf("FAILED to find %s node with id: %ld\n\n", nodeType, id);
            return NULL;
        } else if (node->numConns == 0)
        {
            printf("This node has no links so cannot be used for path finding");
            return NULL;
        }
        
        printf("SUCCESSFULLY found %s node with id: %ld\n\n", nodeType, id);
        return node;
    }
    else
    {
        return NULL;
    }
}

Map* dijkstra(Node* sourceNode, Node* targetNode)
{
    int i;
    float shortestLength = 0;
    long shortestLengthId;
    printf("numComms: %d\n", sourceNode->numConns);
    for (i = 0; i < sourceNode->numConns; i++)
    {
        DijkstraNode* connectedNode = getDijkstraNodeFromId(sourceNode->connList[i].destNodeId);
        long weight = connectedNode->weight;
        if (weight == INFINTIY)
        {
            printf("weight %d\n", weight);
            connectedNode->weight = sourceNode->connList[i].length;
            if (connectedNode->weight > shortestLength)
            {
                printf("weight %d\n", weight);
                shortestLength = connectedNode->weight;
                shortestLengthId = connectedNode->vertexId;
            }
        }
    }
    NodeVisit* closestNode = getNodeVisitFromId(shortestLengthId);
    closestNode->visited = true;
    pathArray[currentPathLength] = shortestLengthId;
    currentPathLength++;
}

DijkstraNode* getDijkstraNodeFromId(long vertexId)
{
    int i;
    for (i = 0; i < distances->length; i++)
    {
        if (distances->dijkstraNodeArray[i].vertexId == vertexId)
        {
            return &distances->dijkstraNodeArray[i];
        }        
    }
    return NULL;
    
}

NodeVisit* getNodeVisitFromId(long vertexId)
{
    int i;
    for (i = 0; i < distances->length; i++)
    {
        if (distances->dijkstraNodeArray[i].vertexId == vertexId)
        {
            return &nodeVisitArray[i];
        }        
    }
    return NULL;
    
}

// function Dijkstra(Graph, source):
// 	    for each vertex v in Graph:	                // Initialization
// 	        dist[v] := infinity	                    // initial distance from source to vertex v is set to infinite
// 	        previous[v] := undefined	            // Previous node in optimal path from source
// 	    dist[source] := 0	                        // Distance from source to source
// 	    Q := the set of all nodes in Graph	        // all nodes in the graph are unoptimized - thus are in Q

// 	    while Q is not empty:	                    // main loop
// 	        u := node in Q with smallest dist[ ]
// 	        remove u from Q
// 	        for each neighbor v of u:	            // where v has not yet been removed from Q.
// 	            alt := dist[u] + dist_between(u, v)
// 	            if alt < dist[v]	                // Relax (u,v)
// 	                dist[v] := alt
// 	                previous[v] := u
// 	    return previous[ ]