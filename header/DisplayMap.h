#ifndef DISPLAY_MAP_DATA_GUARD__H 
#define DISPLAY_MAP_DATA_GUARD__H

#include <stdio.h>
#include "MapStructures.h"

//Prints all nodes and their respective links from the global map
void printNodesAndLinks(Map* g_map);

void printNodesAndLinksText(Map* g_map);

#endif