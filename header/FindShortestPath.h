#ifndef SHORTEST_PATH_GUARD__H 
#define SHORTEST_PATH_GUARD__H

#include <stdio.h>
#include <stdbool.h>
#include "MapStructures.h"

typedef struct {
    long vertexId;
    float weight;
} DijkstraNode;

typedef struct {
    DijkstraNode* dijkstraNodeArray; 
    unsigned int length; 
} Distances;

typedef struct {
    long nodeId; 
    bool visited;
    bool shortestPathFound; 
} NodeVisit;

//Constructor for class, gets node id's of source and target node for shortest path
//Returns pointer to new map if it has been successfully populated with the shortest path between specified nodes
Map* shortest_path();

//Returns node form parsed map for id input from user
//Returns NULL is no node is found with the correct Id or if the node found has no links, otherwise returns a pointer to the node
Node* getNodeFromUser(char* nodeType, Map* map);

//Finds the shortest path between two nodes and populates a map object with the relevant nodes and links
//Returns pointer to map if a shortest path has been found, or a null otherwise
Map* dijkstra(Node* sourceNode, Node* targetNode);

DijkstraNode* getDijkstraNodeFromId(long vertexId);

NodeVisit* getNodeVisitFromId(long vertexId);

#endif