#ifndef MAP_STRUCTURE_GUARD__H 
#define MAP_STRUCTURE_GUARD__H

#include <stdio.h>

typedef struct {
	long linkId;            //Identifier of link
	long destNodeId;        //The conencted node of link
    float length;           //The length of the link in meters
} Link;

typedef struct {
	long id;                //Identifier of node
	float latitude;         //Latitude of node
    float longitide;        //Longitude of node
    unsigned int numConns;  //Number of related connections to node
    Link* connList;        //Poiner to array of all links associated with this node
} Node;

typedef struct{
    Node* nodeArray;       //Pointer to array of all Nodes
    unsigned int length;    //Total num of Nodes
} Map;

//Allocates memory for and populates a Node with specified information 
//Returns pointer to the node if created successfully, or null otherwise
Node* createNodePointer(long id, float latitude, float longitide, unsigned int numConns, Link* connList);

void addToNodeArray(Node* array, int newSize, Node node);

//Allocates memory for and populates a Link with specified information 
//Returns pointer to the link if created successfully, or null otherwise
Link* createLinkPointer(long linkId, long destNodeId, float length);

//Allocates memory for and populates a Map with specified information 
//Returns pointer to the map if created successfully, or null otherwise
Map* createMapPointer(Node* nodeArray, unsigned int length);

//De-allocates all of the memory associated with the specified map object
//Returns 0 if memory was freed successfully, or an error code otherwise
int freeMap(Map* map);

//Finds node from global map by it's ID and returns the pointer to it
//Returns pointer to the node if found, or null otherwise
Node* getNodeFromId(long id, Map* g_map);

#endif