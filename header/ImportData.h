#ifndef IMPORT_DATA_GUARD__H 
#define IMPORT_DATA_GUARD__H

#include <stdio.h>
#include "MapStructures.h"

#define MAXCHAR 512


//Loads all the nodes from the specified file and saves them to the global map
//Returns 0 if nodes were stored correctly, or an error code otherwise
int load_nodes(FILE *file);

//Gets the type of object of the line the pointer is currently pointing at. E.g. link or node
//Returns the string representing the type if one exists, else a null pointer is returned
char* get_type(FILE *file, char type[MAXCHAR]);

//Skips ahead a line of the specified file for the pointer
//Returns 0 if line has been skipped, if all of the specified file has been read then 1 is returned 
int skip_line(FILE *file);

//Loads all the links from the specified file and adds each one to the two relevant node's connectionList's
//Returns 0 if links were stored with their nodes correctly, or an error code otherwise
int load_links(FILE *file);

//Adds specefied links to the relevant node's connectionList's
//Returns 0 if links were stored with their nodes correctly, or an error code otherwise
int addLinkToNode(long nodeId, Link link);

//Neede for unit tests - returns the map
Map* return_map();

//Constructor for class
//Returns pointer to a Map object if class global map has been successfully populated
Map* import_data();

#endif